Tool to update ownership of Tableau workbooks and data sources that are owned by unlicensed users.

## Installation


Create a VENV for safety.

```bash
python3 -m venv venv
source venv/bin/activate
```

Install the requirements.

```bash
pip install -r requirements.txt
```


## Usage


A short help is available when using the `--help` argument

```
$ python script.py --help
usage: script.py [-h] --server SERVER --username USERNAME --password PASSWORD
                 --site SITE --owner OWNER [--dry-run]
                 [--logging-level {debug,info,error}]

Update ownership of workbooks and data sources that are owned by unlicensed users

optional arguments:
  -h, --help            show this help message and exit
  --server SERVER, -s SERVER
                        server address
  --username USERNAME, -u USERNAME
                        username to sign into server
  --password PASSWORD, -p PASSWORD
                        password to sign into server
  --site SITE, -S SITE  site to sign into
  --owner OWNER, -o OWNER
                        user id of the new owner for items owned by unlicensed
                        users
  --dry-run, -d         Dont make any modifications
  --logging-level {debug,info,error}, -l {debug,info,error}
                        desired logging level (set to error by default)
```

* `server` : the server to connect to (including `http://` or `https://`)
* `username` , `password`: the authentication data
* `site`: the site to connect to. use `--site ''` to connect to the Default site
* `dry-run`: dont update anything
* `owner`: the USER ID of the new owner for workbooks with unlicensed owners
