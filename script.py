import tableauserverclient as TSC
import logging
import argparse


def get_unlicensed_user_list(server):
    """ Returns a list of user objects whos users are unlicensed """

    # get all users
    all_users = list(TSC.Pager(server.users))

    # User unlicensed state (2.8+)
    USER_UNLICENSED = "Unlicensed"
    # User unlicensed state (2.8+, 3.0+)
    USER_UNLICENSED_WITH_PUBLISH = "UnlicensedWithPublish"

    # Filter predicate that tells us if a user is unlicensed
    def is_user_unlicensed(user):
        site_role = user.site_role
        return (site_role == USER_UNLICENSED) or (site_role == USER_UNLICENSED_WITH_PUBLISH)

    # Filter the all users list
    return list(filter(is_user_unlicensed, all_users))


def change_owner_of_items_if_unlicensed(all_unlicensed_users, new_user_id, function, is_dry_run=True):
    """ Iterates through items. Function is either `server.datasources` or `server.workbooks` """

    # predicate to check if the user of the workbook is unlicensed
    def is_item_owner_unlicensed(item):
        # The owner of this item
        user_id = item.owner_id
        # Try all users
        for user in all_unlicensed_users:
            if user.id == user_id:
                return True

        # or not
        return False

    logging.debug("Starting to process items")

    # Iterate through the items using the pager
    for item in TSC.Pager(function):

        # Check if this item is owned by an unlicensed user
        if is_item_owner_unlicensed(item):

            # Log the fact that we are changing an item
            logging.info("Found unlicensed item: {0} -- setting owner from {1} to {2}".format(
                item.name, item.id,
                item.owner_id,
                new_user_id
            ))

            # change the owner id to the one supplied
            # TODO: get this from the parent project
            item.owner_id = new_user_id


            # Update the item if needed
            if is_dry_run:
                logging.debug("Skipping update as this is a dry run")
            else:
                # Call update to update the user id
                function.update(item)

            logging.debug("Update done")

    logging.debug("Done processing items")


def main():

    # Arguments
    # =========

    parser = argparse.ArgumentParser(description='Update ownership of workbooks and data sources that are owned by unlicensed users')
    parser.add_argument('--server', '-s', required=True, help='server address')
    parser.add_argument('--username', '-u', required=True, help='username to sign into server')
    parser.add_argument('--password', '-p', required=True, help='password to sign into server')
    parser.add_argument('--site', '-S', required=True, help='site to sign into')
    parser.add_argument('--owner', '-o', required=True, help='user id of the new owner for items owned by unlicensed users')
    parser.add_argument('--dry-run', '-d', help='Dont make any modifications', action="store_true")
    parser.add_argument('--logging-level', '-l', choices=['debug', 'info', 'error'], default='info',
                        help='desired logging level (set to error by default)')

    args = parser.parse_args()

    # Logging
    # =======

    # Set logging level based on user input, or error by default
    logging_level = getattr(logging, args.logging_level.upper())
    logging.basicConfig(level=logging_level)


    # Initial config
    # ==============

    tableau_auth = TSC.TableauAuth(args.username, args.password, args.site)
    server = TSC.Server(args.server)
    new_user_id = args.owner
    is_dry_run = args.dry_run

    with server.auth.sign_in(tableau_auth):

        logging.info("Loading unlicensed user list")

        # Get the user list
        all_unlicensed_users = get_unlicensed_user_list(server)

        logging.info("Found {0} unlicensed users".format(len(all_unlicensed_users)))

        # Change owner of workbooks
        logging.info("Processing workbooks")
        change_owner_of_items_if_unlicensed(all_unlicensed_users, new_user_id, server.workbooks, is_dry_run=is_dry_run)

        # Change owner of datasources
        logging.info("Processing data sources")
        change_owner_of_items_if_unlicensed(all_unlicensed_users, new_user_id, server.datasources, is_dry_run=is_dry_run)




if __name__ == "__main__":
    # execute only if run as a script
    main()
